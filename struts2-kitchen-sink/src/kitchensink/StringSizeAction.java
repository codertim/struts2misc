package kitchensink;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;  


public class StringSizeAction extends ActionSupport implements ServletRequestAware {
	HttpServletRequest request = null;
	private static final long serialVersionUID = 1L;

	
	public String execute() throws Exception {
		Map parameters = ActionContext.getContext().getParameters();
		System.out.println("Map parameters = " + parameters);
		String [] paramStringInputName = (String[]) parameters.get("string-input-name");

		if(paramStringInputName == null) {
			return "input_stuff";   // return SUCCESS;
		} else {
			System.out.println("paramStringInputName = " + paramStringInputName);
			System.out.println("paramStringInputName.length = " + paramStringInputName.length);
			System.out.println("paramStringInputName[0]= " + paramStringInputName[0]);
			int size = paramStringInputName[0].trim().length();
			System.out.println("paramStringInputName size = " + size);
			request.setAttribute("charCount", size);
			return SUCCESS;
		}
		
	}


	public void setServletRequest(HttpServletRequest req) {
		System.out.println("##### setServletRequest - Starting ...");
		request = req;
	}

}
