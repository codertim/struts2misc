package kitchensink;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;  


public class StringCaseAction extends ActionSupport implements ServletRequestAware {
	HttpServletRequest request = null;
	private static final long serialVersionUID = 1L;

	
	public String execute() throws Exception {
		Map parameters = ActionContext.getContext().getParameters();
		System.out.println("Map parameters = " + parameters);
		String [] paramStringInputName = (String[]) parameters.get("string-input-name");

		if(paramStringInputName == null) {
			return "string_input";   // return SUCCESS;
		} else {
			System.out.println("paramStringInputName = " + paramStringInputName);
			System.out.println("paramStringInputName[0]= " + paramStringInputName[0]);
			String caseFinalString = paramStringInputName[0].toLowerCase();
			request.setAttribute("caseFinalString", caseFinalString);
			return SUCCESS;
		}
		
	}


	public void setServletRequest(HttpServletRequest req) {
		System.out.println("##### setServletRequest - Starting ...");
		request = req;
	}

}
