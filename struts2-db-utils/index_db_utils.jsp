<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Welcome</title>
	</head>
	<body>
		<h2>Welcome - Struts 2 Database Utilities</h2>

		<br />
		<br />

		<s:a href="createTablePostgres">Create Table Schema from PostgreSQL DESCRIBE</s:a>

		<br />
		<br />

		<s:a href="createTableMySql">Create Table Schema for MySQL DESCRIBE</s:a>
	</body>
</html>
