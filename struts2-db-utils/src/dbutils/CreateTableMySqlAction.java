
package dbutils;


public class CreateTableMySqlAction {
	public String content;
	public String sqlString = null;

	public String execute() {
		String nextPage     = null;
		String createString = null;

		System.out.println("##### CreateTableMyAction#execute Starting ...");
		System.out.println("##### CreateTableMyAction#execute    content = " + content);

		if(content != null) {
			// customer has submitted input
			createString = parseInput(content);	
			System.out.println("##### CreateTableMySqlAction#execute - createString = " + createString);
			setSqlString(createString);
			nextPage = "success";
		} else {
			// arrive on page, no input yet
			nextPage = "input_create_mysql";
		}
			
		return nextPage;
	}



	private String findTableName(String currentLine) {
		String tableName = null;

		if(currentLine != null) {
			int idx1 = currentLine.toUpperCase().indexOf("DESCRIBE");
			if(idx1 > -1) {
				tableName = currentLine.substring(idx1 + 8, currentLine.length() - 1);
				tableName = tableName.trim();
			} else {
				int idx2 = currentLine.toUpperCase().indexOf("DESC");
				if(idx2 > -1) {
					tableName = currentLine.substring(idx2 + 4, currentLine.length() - 1);
					tableName = tableName.trim();
				}
			}
		}

		return tableName;
	}



	private String parseFieldDetails(String [] fieldDetails) {
		String field          = "";
		int    numDataCounter = 0;   // counter to get two pieces of info: column name and type

		System.out.println("##### CreateTableMySqlAction#parseFieldDetails - Starting ...");
		if(fieldDetails != null) {
			for(int i=0; i < fieldDetails.length; i++) {
				System.out.println("##### CreateTableMySqlAction#parseFieldDetails - fieldDetails[i]" + fieldDetails[i]);
				String currentData = fieldDetails[i].trim();

				if(currentData.trim().length() > 0) {
					numDataCounter++;
					field += currentData;

					if(numDataCounter >=2) {
						field += ", ";
						break;
					} else {
						field += " ";
					}
				}
			}
		}

		return field;
	}



	public String parseInput(String input) {
		boolean isFieldHeadersPassed = false;
		boolean isFieldsStarted = false;
		boolean isTableFound    = false;
		String  createString    = "";
		String  tableName       = "";

		System.out.println("##### CreateTableMySqlAction#parseInput - input = " + input);
		String [] lines = input.split("\n");
		System.out.println("##### CreateTableMySqlAction#parseInput -    number of lines = " + lines.length);

		for(int i=0; i < lines.length; i++) {
			System.out.println("##### CreateTableMySqlAction#parseInput -    current line = |" + lines[i] + "|");
			String currentLine = lines[i].trim();

			if(!isTableFound) {
				tableName = findTableName(currentLine);

				if(tableName != null) {
					isTableFound = true;
				}
			} else {
				// check for fields
				if(isFieldsStarted) {
					String [] fieldDetails = currentLine.split("\\|");
					System.out.println("##### CreateTableMySqlAction#parseInput -    field details = " + fieldDetails);
					System.out.println("##### CreateTableMySqlAction#parseInput -    field details.length = " + fieldDetails.length);
					String fieldDetailsString = parseFieldDetails(fieldDetails);
					createString += fieldDetailsString;
				} else {
					// check for fields section
					if(isFieldHeadersPassed) {
						// one row between headers and fields, so should run this block just once
						System.out.println("##### CreateTableMySqlAction#parseInput -    GOT HERE 1");
						isFieldsStarted = true;
					} else {
						System.out.println("##### CreateTableMySqlAction#parseInput -    GOT HERE 2");
						if(currentLine.startsWith("| Field")) {
							isFieldHeadersPassed = true;	
						}
					}
				}
			}

		}

		System.out.println("##### CreateTableMySqlAction#parseInput -    table name = |" + tableName + "|");

		return createString;
	}



	public void setContent(String c) { this.content = c; }

	public String getContent() {  return this.content; }


	public void setSqlString(String s) { this.sqlString = s; }

	public String getSqlString() {  return this.sqlString; }


	

/*
  EXAMPLE INPUT

mysql> desc assets;
+--------------------+--------------+------+-----+---------+----------------+
| Field              | Type         | Null | Key | Default | Extra          |
+--------------------+--------------+------+-----+---------+----------------+
| id                 | int(11)      | NO   | PRI | NULL    | auto_increment |
| caption            | varchar(255) | YES  |     | NULL    |                |
| title              | varchar(255) | YES  |     | NULL    |                |
| asset_file_name    | varchar(255) | YES  |     | NULL    |                |
| asset_content_type | varchar(255) | YES  |     | NULL    |                |
| asset_file_size    | int(11)      | YES  |     | NULL    |                |
| created_by_id      | int(11)      | YES  |     | NULL    |                |
| updated_by_id      | int(11)      | YES  |     | NULL    |                |
| created_at         | datetime     | YES  |     | NULL    |                |
| updated_at         | datetime     | YES  |     | NULL    |                |
| uuid               | varchar(255) | YES  |     | NULL    |                |
+--------------------+--------------+------+-----+---------+----------------+
*/

}

