
package dbutils;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;  


public class CreateTablePostgresAction extends ActionSupport implements ServletRequestAware {
        HttpServletRequest request = null;
        private static final long serialVersionUID = 1L;



        public String execute() throws Exception {
                Map parameters = ActionContext.getContext().getParameters();
                System.out.println("Map parameters = " + parameters);
                String [] paramContent = (String[]) parameters.get("content");

                if(paramContent == null) {
                        return "input_stuff";
                } else {
                        System.out.println("paramContent= " + paramContent);
                        System.out.println("paramContent.length = " + paramContent.length);
                        System.out.println("paramcontent[0]= " + paramContent[0]);
                        int size = paramContent[0].trim().length();
                        System.out.println("total - paramStringInputName size = " + size);
						String sqlTable = getTableName(paramContent[0]);   // "create table blahblah";

						String [] inputLines = paramContent[0].split("\n");
						System.out.println("inputLines length = " + inputLines.length);
						String allColString = constructAllColString(inputLines);
						System.out.println("allColString = " + allColString);

						String sqlCommand = makeSqlCreateQuery(sqlTable, allColString);
                        request.setAttribute("sql_ddl", sqlCommand);
                        return SUCCESS;
                }

        }



		// combine table name with field names
		private String makeSqlCreateQuery(String table, String cols) {
			String sql = "create table ";

			if(table != null) {
				sql += (" " + table + " " + cols);
			}

			String sqlTrimmed = sql.trim();

			String finalSql = sqlTrimmed;
			if(sqlTrimmed.endsWith(",")) {
				// chop off last character if comma
				finalSql = sqlTrimmed.substring(0, (sqlTrimmed.length() - 1));
			}

		    return finalSql;	
		}



        public void setServletRequest(HttpServletRequest req) {
                System.out.println("##### setServletRequest - Starting ...");
                request = req;
        }



		// combine all columns with their types into a single string
		private String constructAllColString(String [] allLines) {
			String allColString ="";

			if(allLines != null) {
				boolean isColSection = false;

				// look at the lines to get column/field info
				for(int i=0; i < allLines.length; i++) {
					String currentLine = allLines[i];
					System.out.println("     currentLine = " + currentLine);
					if(isColSection) {
						String [] fields = currentLine.split("\\|");
						if( (fields != null) && (fields.length >= 3)) {
							System.out.println("     fields[0] = " + fields[0]);
							System.out.println("     fields.length = " + fields.length);
							String currentCol = fields[0].trim();
							String currentType = fields[1].trim();
							// TODO: change to StringBuffer
							allColString += (" " + currentCol + " " + currentType);

							if(i <= allLines.length ) {
								// separate columns with comma
								allColString += ",";
							}
						}

					} else {
						if(currentLine.indexOf("Column") >= 0) {
							isColSection = true;   // the rest of the lines should be columns
						}
					}
				}
			} else {
				System.out.println("##### ERROR - constructAllColString - ERROR - line arrary is null");
			}
			
			return allColString;
		}


		// take input from web from that user copy/pasted from sql schema, e.g. from copyin "\d mytable" in postgresql
		private String getTableName(String userInput) {
			String tableName = "";

			if(userInput != null) {
				String [] lines = userInput.split("\n");
				if(lines != null) {
					for(int i=0; i < lines.length; i++) {
						System.out.println("Current line: " + lines[i]);
						int matchIndex = lines[i].indexOf("Table \"public");
						if(matchIndex > -1) {
							// we have a match: found line with table name
							System.out.println("  Start Index = " + matchIndex);
							int endIndex = lines[i].indexOf("\"", (matchIndex+14));
							System.out.println("  End Index = " + endIndex);
							int startIndex = matchIndex + 14;
							tableName = lines[i].substring(startIndex, endIndex);
						}
					}		
				}
			}

			System.out.println("##### getTableName - returning table name = |" + tableName + "|");

			return tableName;
		}
}

