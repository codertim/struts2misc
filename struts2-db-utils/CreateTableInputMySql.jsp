<html>
	<head>
		<title>Create Table</title>
	</head>
	<body>
		<form action="createTableMySql" method="post">
			<div>
				<textarea name="content" rows="25" cols="90">  

mysql> desc assets;
+--------------------+--------------+------+-----+---------+----------------+
| Field              | Type         | Null | Key | Default | Extra          |
+--------------------+--------------+------+-----+---------+----------------+
| id                 | int(11)      | NO   | PRI | NULL    | auto_increment |
| caption            | varchar(255) | YES  |     | NULL    |                |
| title              | varchar(255) | YES  |     | NULL    |                |
| asset_file_name    | varchar(255) | YES  |     | NULL    |                |
| asset_file_size    | int(11)      | YES  |     | NULL    |                |
| updated_at         | datetime     | YES  |     | NULL    |                |
+--------------------+--------------+------+-----+---------+----------------+
11 rows in set (0.00 sec)


				</textarea></div>
			<br />
			<div><input type="submit" value="Parse Schema"></div>
		</form>
	</body>
</html>

